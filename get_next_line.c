#include	"./includes/ft.h"

t_x	*	tr_create_list(char c, int i)
{
	t_x	*	new;

	if (!(new = (t_x*)malloc(sizeof(t_x))))
		return (NULL);
	new->c = c;
	new->n = i;
	new->left = NULL;
	new->right = NULL;
	new->back = NULL;
	return (new);
}

void	tr_clear_list(t_x *list)
{
	(void)list->c;
	(void)list->n;
	list->left = NULL;
	list->right = NULL;
	list->back = NULL;
	free(list);
	list = NULL;
}

int		tr_push_list(t_x **lists, t_x *data)
{
	t_x	*	new;

	new = *lists;
	if (!data)
		return (1);
	else
	{
		new->n++;
		if (new->left)
		{
			new->left->back = data;
			data->left = new->left;
			data->back = new;
			new->left = data;
		}
		else
		{
			data->back = new;
			new->left = data;
		}
	}
}

void	swap_lists(t_x **lists)
{
	t_x	*	swap;

	swap = tr_create_list('\0', 0);
	(*lists)->back = swap;
	swap->right = *lists;
	*lists = swap;
	tr_push_list(lists, tr_create_list((char)26, 1));
}

char	*	get_str(t_x *lists, int flag)
{
	t_x				*	next;
	static	char	*	str;
	static	int			n = 0;

	next = lists;
	while (next->right)
		next = next->right;
	if (flag)
		str = ft_strnew((size_t)next->n - 1);
	if (next->left)
		get_str(next->left, 0);
	if (next->c != (char)26)
	str[n++] = next->c;
	if (flag)
		n = 0;
	if (next->back && next->back->right)
		next->back->right = NULL;
	tr_clear_list(next);
	return (str);
}

int		get_next_line(const int fd, char **line)
{
	int				n;
	char			buf[BUFF_SIZE + 1];
	static	t_x	*	lists = NULL;
	int				flag;

	if (fd < 3 || !line)
		return (-1);
	flag = 0;
	while (!flag && (n = read(fd, buf, BUFF_SIZE)))
	{
		if (!lists)
			lists  = tr_create_list('\0', 1);
		buf[n] = (char)26;
		n = -1;
		while (buf[++n] != 26)
			if (buf[n] == '\n' && ++flag)
				swap_lists(&lists);
			else
				tr_push_list(&lists, tr_create_list(buf[n], 1));
	}
	*line = get_str(lists, 1);
	if (n < BUFF_SIZE && !lists->left)
		return (0);
	return (1);
}
