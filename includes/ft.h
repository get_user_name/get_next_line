#ifndef		FT_H
# define	FT_H

# include	<fcntl.h>
# include	"./libft.h"

# define END(lists) lists = NULL; return (0);
# define BUFF_SIZE 5

typedef struct		s_x
{
	char			c;
	int				n;
	struct	s_x		*left;
	struct	s_x		*right;
	struct	s_x		*back;
}					t_x;

int		get_next_line(const int fd, char **line);

#endif