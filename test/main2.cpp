#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main() {
    ifstream ifs( "myfile.txt", ios::binary );
    string line;
    while( getline( ifs, line ) ) {
        cout << line << '\n';
    }
}